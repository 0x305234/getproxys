#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from threading import Thread
from queue import Queue

import json
import requests
import sys

class getProxys(object):
    ''' Get proxys from sites sslproxies.org, socks-proxy.net, free-proxy-list.net for
    now ,test proxys, format the data returns json or list '''

    def __init__(self, proxytype=None, overproxy=None, useragent=None):
        self.proxytype = proxytype
        self.overproxy = overproxy
        self.useragent = useragent

        self.testurl = 'https://ifconfig.me/all.json'

        self.myinfo = self.whoAmI()
        self.proxysoup = BeautifulSoup(self.__requester(self.__setUrl(), self.overproxy).text, 'lxml')
        self.proxydata = self.__html2data(self.proxysoup)

    def cit(self, color, string):
        ''' string coloring  '''
        colors = {'G':'\033[92m', 'Y':'\033[93m', 'R':'\033[91m', 'END':'\033[0m'}
        return colors[color] + str(string) + colors['END']

    def __setUrl(self):
        ''' sets proxy type '''
        # sets url for crawl
        urls = {'ssl':'https://www.sslproxies.org/',
                'socks':'https://www.socks-proxy.net/',
                'anon':'https://free-proxy-list.net/anonymous-proxy.html'
                }
        if self.proxytype == 'ssl':
            return urls['ssl']

        elif self.proxytype == 'socks':
            return urls['socks']

        elif self.proxytype == 'anon':
            return urls['anon']

        else:
            return urls['ssl']

    def __requester(self, url, overproxy=None):
        ''' request data and return soup '''

        headers = {'user-agent': self.useragent }
        proxy = {'http':'{}'.format(overproxy),
                 'https':'{}'.format(overproxy)}

        if overproxy:
            try:
                return requests.get(url, headers=headers, timeout=15, proxies=proxy, allow_redirects=False)
            
            except requests.exceptions.Timeout:
                print(self.cit('Y', 'Request Timeout'), 'proxy:{}'.format(overproxy))

            except requests.exceptions.ProxyError:
                print(self.cit('R', 'Proxy Error'), 'proxy:{}'.format(overproxy))

            except requests.exceptions.SSLError:
                print(self.cit('Y', 'SSL Error'), 'proxy:{}'.format(overproxy))

            except TimeoutError:
                print(self.cit('Y', 'Read Timeout'), 'proxy:{}'.format(overproxy))

            except requests.exceptions.ConnectionError as err:
                print('proxy:{}'.format(err))

            return None
        else:
            return requests.get(url, headers=headers, timeout=15, allow_redirects=False) 

    def __testProxy(self, proxy):
        ''' test if proxy works '''
        proxyt = self.__requester(self.testurl, proxy)
        myip = self.myinfo['ip_addr']
        if proxyt:
            if myip != json.loads(proxyt.text)['ip_addr']:
                print(self.cit('G','Proxy Ok'), 'proxy:{}'.format(proxy))
                return proxy
            else:
                print('Proxy {} didnt work.'.format(proxy))
                return None
        else:
            return None

    def __html2data(self, soup):
        ''' read html and return proxy info '''
        content = []
        table = soup.find('table', attrs={'id': 'proxylisttable'})

        tableHead = table.find('thead')
        tableHeadData = tableHead.find_all('th')

        # need smarter move here
        headData = []
        for el in tableHeadData:
            headData.append(el.get_text(strip=True))

        tableBody = table.find('tbody')
        tableBodyData = tableBody.find_all('tr')

        for tabledata in tableBodyData:
            itemData = {}
            headIter = iter(headData)

            for element in tabledata.find_all('td'):
                itemData[next(headIter)] = element.get_text(strip=True)
                
            content.append(itemData)
        return content

    def testProxys(self, proxylist=None):
        ''' return working proxys '''
        # if u want to use your proxys pass a list of them as proxylist
        if proxylist:
            proxysToTest = proxylist
        else:
            proxysToTest = ['{}:{}'.format(proxy['IP Address'], proxy['Port']) for proxy in self.proxydata]

        workingRawProxys = [{} for _ in proxysToTest]
        num_thread = min(100, len(proxysToTest))
        que = Queue(maxsize=1000)

        def startWorkers(que, workingRawProxys, num_thread):
            ''' fire up th '''
            for thread in range(num_thread):
                workerthread = Thread(target=worker, name='worker{}'.format(thread), args=(que,workingRawProxys))
                workerthread.setDaemon(True)    
                workerthread.start()
            # now we wait 
            que.join()
            print('All tasks completed, returning resaults.')
            return workingRawProxys

        def worker(que, result):
            ''' yes it gets the job done '''
            while not que.empty():
                job = que.get_nowait()
                try:
                    data = self.__testProxy(job[1])
                    result[job[0]] = data
                except:
                    print(self.cit('R','Error with URL check!'))
                    result[job[0]] = {}
                # signal to the queue that task has been processed
                que.task_done()
            return True

        def filterProxys(resaults):
            ''' filter data '''
            workingproxys = []
            for item in resaults:
                if item:
                    proxy = item.split(':')[0]
                    for proxyinfo in self.proxydata:
                        if proxyinfo['IP Address'] == proxy:
                            workingproxys.append(proxyinfo)
            return workingproxys

        print('Testing Proxys :{}'.format(len(proxysToTest)))
        for index in range(len(proxysToTest)):
            que.put((index, proxysToTest[index]))

        startWorkers(que, workingRawProxys, num_thread)

        if not proxylist:
            workingproxys = filterProxys(workingRawProxys)
            return workingproxys
        else:
            return workingRawProxys
            
    def whoAmI(self):
        ''' info about real ip '''
        return json.loads(self.__requester(self.testurl).text)

    def returnProxys(self):
        ''' return proxydata '''
        return self.proxydata

    def returnWorkingProxys(self):
        ''' return working(tested) proxydata '''
        return self.testProxys()
