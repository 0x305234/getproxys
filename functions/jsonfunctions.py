import json
import os

# fpname filename and path to it
def genJson(data, fpname):
    ''' generate json with proxys '''
    with open(fpname, 'w') as jsonfile:
        json.dump(data, jsonfile, indent=2)
    return fpname

def json2data(file):
    ''' read json / import proxys '''
    with open(file) as jsonfile:
        data = json.load(jsonfile)
    return data
