import lxml
import xml.etree.ElementTree as etree

def genXml(data, fpname):
    ''' generate xml with proxys '''
    root = etree.Element('root')

    # iterate over items
    for iid, item in enumerate(data):
        doc = etree.SubElement(root, 'item-{}'.format(iid+1))
        
        for key, value in item.items():
            etree.SubElement(doc, key.replace(' ','-')).text = value

    # creating xml 
    tree = etree.ElementTree(root)
    tree.write(fpname, encoding='utf8')
    return fpname

def xml2data(file):
    ''' read xml / import proxys '''
    parser = lxml.etree.XMLParser(recover=True)
    tree = lxml.etree.parse(file, parser=parser)
    root = tree.getroot()

    itemList = []
    for items in root:
        itemsDict = {}
        for item in items:
            itemsDict[item.tag] = item.text

        itemList.append(itemsDict)
    return itemList