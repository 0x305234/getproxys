# just testing
import os
import sys
import argparse

from getproxys.gp import getProxys
from functions.jsonfunctions import genJson, json2data
from functions.xmlfunctions import genXml, xml2data
from functions.printfunctions import prettyPrint
from var.banner import banner

whereami = os.path.dirname(os.path.abspath(__file__))
vardir = os.path.join(whereami, 'var')

def argparser():
    parser = argparse.ArgumentParser(description="Get proxys from sites sslproxies.org, socks-proxy.net, free-proxy-list.net for now\
        ,test proxys, format the data returns json or list in short :)",
    epilog='Example of use : python3.6 get_proxys.py -o 1.1.1.1:33 -t anon.')

    parser.add_argument("-t", "--proxytype", type=str, metavar="", default="ssl", choices=['ssl', 'anon', 'socks'],
    help="Set specific proxy type (ssl, anon, socks) | Default is ssl")

    parser.add_argument("-o", "--overproxy", type=str, metavar="", default=None,
    help="Get some proxys over some proxys -o ip:port")

    parser.add_argument("-p", "--printdata", type=str, metavar="", default=None, choices=['all', 'ip:port'],
    help="Print data. For specific print you can (all, ip:port)")

    parser.add_argument("-Tp", "--testproxys", action="store_true",
    help="Test proxys.")

    parser.add_argument("-x", "--xml", type=str, metavar="", nargs='?', const=vardir,
    help="Write data to xml file.")

    parser.add_argument("-j", "--json", type=str, metavar="", nargs='?', const=vardir,
    help="Write data to json.")

    if len(sys.argv) == 1:
        print(banner)
        parser.print_help(sys.stderr)
        sys.exit(1)

    return parser.parse_args()

if __name__ == '__main__':
    args = argparser()
    printdata = args.printdata

    ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    gp = getProxys(proxytype=args.proxytype, overproxy=args.overproxy, useragent=ua)

    if args.testproxys:
        data = gp.returnWorkingProxys()
    else:
        data = gp.returnProxys()

    if printdata:
        prettyPrint(printdata, data)

    if args.json:
        if args.json == vardir:
            genJson(data, os.path.join(vardir, 'proxys.json'))
        else: 
            genJson(data, args.json)

    if args.xml:
        if args.xml == vardir:
            genXml(data, os.path.join(vardir, 'proxys.xml'))
        else:
            genXml(data, args.xml)

