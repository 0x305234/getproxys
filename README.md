get_proxys.py / Get proxys, test them, use them.
============

![gp](/var/gp.png "get_proxys")

Key Features
------------

  * Get proxys from (sslproxies.org, socks-proxy.net, free-proxy-list.net)
  * Returns (data, json, xml)
  * Test proxys


How to use
------------
~~~bash
# Show help
python3 get_proxys.py -h

# Get anon proxys over 1.2.3.4:56 proxy and print resaults
python3 get_proxys.py -t anon -o '1.2.3.4:56' -p

# Get socks proxys test them print ip:port and generate json
python3 get_proxys.py -t socks -Tp -p 'ip:port' -j
~~~

How to use getProxys object
------------

Test proxy list.
~~~python
from getproxys.gp import getProxys

ua = 'some-ua'
proxystotest = ['1.2.3.4:5', '2.2.3.4:5', '3.2.3.4:5']

gp = getProxys(useragent=ua)
print(gp.testProxys(proxystotest)) # need list here
~~~

Return formated data for socks proxys.
~~~python
from getproxys.gp import getProxys

ua = 'some-ua'
gp = getProxys(useragent=ua)
print(gp.returnProxys())
~~~


Requirements
------------

  * beautifulsoup4
  * lxml
  * requests

Todo
------------

  * Docker
  * More print options
  * Dynamic UA
